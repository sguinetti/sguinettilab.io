---
layout: page
title: About
permalink: /about/
feature-img: "img/sample_feature_img_2.png"
---

Hola, soy Diego. Estudiante y aficionado de tecnología.

# Breve biografía

Soy peruano de nacimiento. Viví unos años en Lima y más tarde a la selva. Cursé en el colegio [Sollertia](https://es.wikipedia.org/wiki/Sollertia), (básico, gastronomía, ajedrez, computación), CADES (2009), EIGER (2011-2016), [Nueva Acrópolis](https://es.wikipedia.org/wiki/Nueva_Acrópolis) (2013) e Hipólito Unanue (2017).

# Habilidades
Me oriento a la tecnología y el uso responsable en la vida digital. Soy más de software para usuarios finales con proyecciones hacia el código abierto, control de datos privados y tecnología descentralizada.

Me gusta la informática. Dominé Windows 98, pasando por XP, después por 7 y 10. Aprendí GNU/Linux, destacando a Ubuntu y mi favorito Deepin. Colaboré en Deepin en Español, comunidad relacionada a este sistema.

Domino programación. Los más importantes son el PHP, SQL, JavaScript, CSS y HTML5. Aprendí a usar Wordpress, Jekyll y MediaWiki. Estoy programando en Python, que sucede a Visual Basic.

Tengo habilidad de redacción con participar indirectamente con Wikipedia. Desde 2010 tengo cierta comunicación, que estudié gran parte en EIGER. Tengo afición al japonés.

# Aficiones

Me encanta la fotografía, los paisajes y diseño. Tengo una filosofía algo idealista, busco ser un yo más personal.

Colaboré en Wikipedia (noviembre de 2009) para el artículo [Pucallpa](https://es.wikipedia.org/wiki/Pucallpa) y el [asistente de artículos](https://es.wikipedia.org/wiki/WP:ACA). También en Commons con la [regulación de los derechos de autor en Perú](https://commons.wikimedia.org/wiki/COM:Peru).

Paricipé en [Openstreetmap](https://es.wikipedia.org/wiki/Openstreetmap) y Mapillary para recuperar detalles de Perú. Además de la elaboraciones de redes de transporte público.

Leo libros, recuerdo que la primera vez fue Gallinazo sin plumas. También me encanta ver películas con finales intrigantes y emocionantes. Busco en la redacción de ciencia ficción.

Soy melómano, con tendencia a la música armónica y clásica. Soy de géneros frescos como el rock alternativo, el jazz y la fusión (véase "Awiwa Plus").

## Extra

No uso Facebook. Tengo un [canal de Telegram](https://telegram.me/sguinetti), en donde colecto enlaces de la nada.
