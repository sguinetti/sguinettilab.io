---
layout: post
title: Lista de aplicaciones opensource para Android traducidas
feature-img: "img/sample_feature_img_3.png"
---

Esta página es un registro personal que ayuda a los hispanohablantes tener software para móvil de calidad. El catálogo menciona una descripción y el código fuente donde está alojado. Este no incluye las recomendada de F-Droid.

## Lista de aplicaciones

Las aplicaciones son independientes y sus traducciones al español fueron aceptadas por iniciativa del autor. Ordenado casi cronológicamente:

### Números
* [2048 Open Fun Game](https://github.com/andstatus/game2048) (juego de 2048)
* [3D Model Viewer](https://github.com/the3deers/android-3D-model-viewer) (visor de objetos 3D)

### A-M
* [Anglers' Log](https://github.com/cohenadair/anglers-log) (registro de pesca de río o mar, F-Droid)
* [Axet apps](https://gitlab.com/axet) (set de aplicaciones básicas, F-Droid)
* [Aard](https://github.com/itkach/aard2-android) (dictionary)
* [AlwaysOn](https://github.com/Domi04151309/AlwaysOn) (estilo para pantallas AMOLED, F-Droid)
* [APK Explorer Editor](https://github.com/apk-editor/APK-Explorer-Editor) (editor de instaladores APK a lo ingeniería inversa, F-Droid)
* [Baby Dots](https://github.com/babydots/babydots) (juego para bebés, F-Droid)
* [Binary Eye](https://github.com/markusfisch/BinaryEye) (lector QR, F-Droid)
* [bodyweight](https://github.com/mazurio/bodyweight-fitness-android) (registro físico, F-Droid)
* [CamelCamelCamel](https://github.com/ebaschiera/TripleCamel) (scrapping para el portal web que lleva ese nombre)
* [CaveSurvey](https://github.com/lz1asl/CaveSurvey) (inspección en cuevas)
* [Cinelog](https://github.com/Alcidauk/CineLog) (reseñas de películas y TV, F-Droid)
* [Cryptocam](https://gitlab.com/cryptocam/cryptocam/) (filmación cifrada "espía", F-Droid)
* [Cuppa](https://github.com/ncosgray/cuppa_mobile) (cronómetro para el té, F-Droid)
* [CUPS](https://github.com/BenoitDuffez/AndroidCupsPrint) (enviar a la impresora, F-Droid)
* [Dart Checker](https://gitlab.com/onestepb/dart-checker) (juego de dardos, F-Droid)
* [DecSyncCC](https://github.com/39aldo39/DecSyncCC) (sincronización entre móvil y PC, F-droid)
* [DeepL](https://github.com/sakusaku3939/DeepLAndroid) (traductor, F-droid)
* [EasyWatermark](https://github.com/rosuH/EasyWatermark) (creador de marcas de agua, inicialmente disponible en Chino, F-Droid)
* [EnRecipes](https://github.com/vishnuraghavb/EnRecipes) (organizador de cocina e ingredientes)
* [Eggtimer](https://github.com/woheller69/eggtimer) (medición de cocción de huevo)
* [EweSticker](https://github.com/FredHappyface/Android.EweSticker) (selector de pegatinas)
* [FastoTV Lite](https://github.com/fastogt/fastotvlite_mobile) (cliente IPTV, Play Store)
* [Fake Contacts](https://github.com/BillDietrich/fake_contacts/) (utilitario de seguridad contra extracción de contactos, F-Droid)
* [Feeel](https://gitlab.com/enjoyingfoss/feeel) (rutina de ejercicio, F-Droid)
* [Finder](https://github.com/Seva-coder/Finder) (buscador de teléfono de SMS, F-Droid)
* [Fluent Reader](https://github.com/yang991178/fluent-reader-lite/) (lector RSS multiservicio, F-Droid)
* [Frost](https://github.com/dkanada/frost/) (set de iconos, F-Droid)
* [FWallet](https://gitlab.com/TheOneWithTheBraid/f_wallet) (billetera virtual, F-Droid)
* [HeartBeat](https://github.com/berdosi/HeartBeat) (medida alternativa de pulso cardíaco, F-Droid)
* [HexViewer](https://github.com/Keidan/HexViewer/) (visor de texto hexadecimal para programadores)
* [HomeApp](https://github.com/Domi04151309/HomeApp) (domótica, F-Droid)
* [Hypezig](https://gitlab.com/intergalacticsuperhero/hypezig) (scrapping para eventos de Facebook o en formato ical)
* [Images to PDF](https://github.com/Swati4star/Images-to-PDF) (creador de archivos PDF a partir de imágenes o texto con opciones de visualización y de arreglos de páginas, F-Droid)
* [Instagrabber](https://gitlab.com/AwaisKing/instagrabber) (cliente de Instagram,  F-droid)
* [IR Remote](https://gitlab.com/divested-mobile/irremote) (control o mando remoto infrarrojo, F-droid)
* [Key Mapper](https://github.com/sds100/KeyMapper) (accesibilidad, F-Droid)
* [Kinoko](https://github.com/nv95/Kotatsu) (lector de historietas orientales, manga, F-Droid)
* [Kotatsu](https://github.com/nv95/Kotatsu) (lector de historietas orientales, manga, F-Droid)
* [Libra](https://github.com/markusfisch/Libra) (toma de decisiones basada en la correcta elección si el valor positivo es el doble que el negativo, F-Droid)
* [Librery](https://gitlab.com/harisont/Librery/) (librería,  F-droid)
* [LibreTrack](https://github.com/proninyaroslav/libretrack) (seguimients de paquetes UPS,  F-droid)
* [M3UAndroid](https://github.com/oxyroid/M3UAndroid/) (cliente IPTV con soporte M3U, F-droid)
* [Metronome](https://github.com/thetwom/toc2) (metrónomo)
* [microMathematics](https://github.com/mkulesh/microMathematics) (cas, F-Droid)
* [Migraine Logo](https://gitlab.com/zerodogg/org.zerodogg.migraineLog) (herramienta para registrar síntomas de migraña y otros dolores de cabeza, F-Droid)
* [moneywallet](https://github.com/AndreAle94/moneywallet) (monedero, F-Droid)

### N-Z
* [Ning](https://github.com/csicar/Ning) (monitoreo de instrusos en redes Wi-Fi)
* [ncalc](https://github.com/tranleduy2000/ncalc) (calculadora avanzada, Play Store)
* [Nunti](https://gitlab.com/ondrejfoltyn/nunti) (lector RSS con sugerencias locales, F-Droid)
* [Ohms Now](https://github.com/ktprograms/Ohms_Now) (calculador de resistores, F-Droid)
* [PCAPDroid](https://github.com/emanuele-f/PCAPdroid) (monitreo y volcado red, con tecnología MITM, F-Droid)
* [Planisphere](https://github.com/tengel/AndroidPlanisphere) (planetario, F-Droid)
* [Plees Tracker](https://github.com/vmiklos/plees-tracker/) (monitoreo de sueño, F-droid)
* [PocketMaps](https://github.com/junjunguo/PocketMaps) (cliente de mapas basado en OSM)
* [PulseMusic](https://github.com/HardcodeCoder/PulseMusic) (reproductor de música)
* [SafeCharge](https://github.com/AswinChand97/SafeCharger) (notificación de carga eléctrica segura)
* [Sumatora](https://github.com/HappyPeng2x/SumatoraDictionary) (diccionario de japones, solo aporte en las líneas de traducción)
* [Scoop](https://github.com/TacoTheDank/Scoop) (herramienta de rastreo de pila para encontrar la causa del bloqueo)
* [Prepaid Balace](https://github.com/mueller-ma/PrepaidBalance/) (registro de saldo de tarjetas prepago vía USSD, F-Droid)
* [PilferShushJammer](https://github.com/kaputnikGo/PilferShushJammer) (bloqueador para grabaciones)
* [Price Per Unit](https://github.com/brentvollebregt/price-per-unit) (consulta de precios de productos de mercado)
* [Tape Measure](https://github.com/SecUSo/privacy-friendly-tape-measure) (regla virtual, F-droid)
* [Taskbar](https://github.com/farmerbb/Taskbar) (barra de tareas para tablets)
* [Techahashi](https://github.com/andswitch/techahashi) (creación rápida de diapositivas, F-droid)
* [Thumb-key](https://github.com/dessalines/thumb-key) (teclado no convencional que muestra menos teclas para realizar varias funciones, F-droid)
* [Track and Graph](https://github.com/SamAmco/track-and-graph/) (creación de gráficos)
* [TrackerControl](https://github.com/OxfordHCC/tracker-control-android) (VPN para controlar rastreadores de apps, F-droid)
* [Twire](https://github.com/Perflyst/Twire/) (cliente de Twich de teléfono de SMS, F-Droid)
* [WaveLinesWallpaper](https://github.com/markusfisch/WaveLinesWallpaper) (generador de fondos de pantalla)
* [YetAnotherCallBlocker](https://gitlab.com/xynngh/YetAnotherCallBlocker) (enviar a la impresora, F-Droid)

## Menciones especiales

Menciones adiciones de aplicaciones que la comunidad lo tradujo pero tienen buena calidad (y probablemente no está mencionada en la selección de apps de F-Droid):

* [Birday](https://github.com/m-i-n-a-r/birday) (recordatorio de cumpleaños, F-Droid; aplicación similar: [Birthday](https://gitlab.com/tmendes/BirthDayDroid/))
* [Trail Sense](https://github.com/kylecorry31/Trail-Sense) (correccciones de error, herramientas físicas del celular para el clima)
* [OpenPods](https://github.com/adolfintel/OpenPods) (accesorio para AirPods, F-Droid)
* [AntenaPod](https://github.com/AntennaPod/AntennaPod) (podcast)
* [Koler](https://github.com/Chooloo/call_manage) (llamadas en línea, F-Droid)
* [Scrambled Exif](https://www.transifex.com/juanitobananas/scrambled-exif/) (eliminar metadatos de imagen)
* [FitoTrack](https://codeberg.org/jannis/FitoTrack) (actividades al aire libre)
* [TheLife](https://gitlab.com/hotlittlewhitedog/BibleTheLife) (Biblia)
* [2048](https://github.com/uberspot/2048-android) (juego)
* [J2ME](https://f-droid.org/es/packages/ru.playsoftware.j2meloader) (emulador para la antigua plataforma de celulares)
* [Moonlight](https://github.com/moonlight-stream/moonlight-android) (característica de streaming para Nvidia)
* [ChangeDetection](https://github.com/bernaferrari/ChangeDetection) (revisión de cambios de páginas web)
* [Untrackme](https://framagit.org/tom79/nitterizeme) (redirección de redes sociales a clientes web no rastreables, F-droid)
* [VectorifyDaHome](https://github.com/enricocid/VectorifyDaHome) (creación de fondos de pantalla, F-droid)
* [icsx5](https://gitlab.com/bitfireAT/icsx5) (compatibilidad, ics)
* [Ameixa](https://gitlab.com/xphnx/ameixa) (paquete de iconos)
* [Navi](https://github.com/TachibanaGeneralLaboratories/download-navi) (descargas, F-droid)
* [PlainUPnP](https://github.com/m3sv/PlainUPnP) (transmisión PnP Google Play, por ahora no hay archivos de traducción)
* [Drumon](https://github.com/tube42/drumon) (juego de batería digital, F-droid)
* [Replash](https://github.com/b-lam/Resplash/) (cliente Unplash, Google Play)
* [Open Note Scanner](https://github.com/ctodobom/OpenNoteScanner/) (escáner y conversor a PDF basado en OpenCV Manager, Google Play)
* [Lynknet](https://github.com/arunkumar9t2/lynket-browser) (navegador web, centrado en las pestañas, F-Droid)
* [Goodtime](https://github.com/adrcotfas/Goodtime) (pomodoro)
* [OpenBoard](https://github.com/dslul/openboard) (teclado)
* [Remote Desktop](https://github.com/iiordanov/remote-desktop-clients) (cliente aRDP y bVNC, F-Droid)
* [LibreAV](https://github.com/projectmatris/antimalwareapp) (servicio antimalware)
* [Download Navi](https://github.com/TachibanaGeneralLaboratories/download-navi/) (gestor de descargas)
* [Diario de Contactos](https://github.com/apozas/contactdiary) (gestión de contactos sospechosos de enfermedades de contagio, diseñado para la prevención del Covid-19)
* [Semitone](https://github.com/tckmn/semitone) (piano metrónomo, F-Droid)
* [Quinb](https://gitlab.com/DeepDaikon/Quinb) (juego social de trivia)
* [TagSpaces](https://www.tagspaces.org/) (recolector y anotador de notas de código abierto, compatible con escritorio y sincronizable)
* [OCR App](https://github.com/SubhamTyagi/android-ocr) (reconocimiento visual de texto)
* [Stock Ticker Widget](https://github.com/premnirmal/StockTicker/tree/master/app/src/main/res) (widget para ver acciones económicas de las empresas en bolsa, F-Droid)
* [Just Video Player](https://github.com/moneytoo/Player) (reproductor multimedia simple con PiP y soporte de varios códecs, F-Droid)
* [Onion Viewer](https://github.com/max-kammerer/orion-viewer) (visor de documentos, F-Droid)
* [Snapdroid](https://github.com/fm-sys/snapdrop-android) (cliente para compartir, F-Droid y Google Play)
* [Diaguard](https://github.com/Faltenreich/Diaguard) (diario para control de diabetes, F-Droid)
* [TV Player Android](https://github.com/cy8018/TVPlayer.Android) (cliente IPTV, Google Play)
* [Randomix](https://github.com/m-i-n-a-r/randomix) (aplicación para generar respuestas aleatoreas)
* [SeriesGuide](https://github.com/UweTrottmann/SeriesGuide) (guía de series de televisión)
* [Docus](https://github.com/Breta01/docus) (simple gestor de documentos escaneados)
* [Organic Maps](https://github.com/organicmaps/organicmaps/) (basado en Maps.me sin añadidos de la empresa para enfocarse en una simple app de maps)
* [Stealth](https://gitlab.com/cosmosapps/stealth) (cliente de Reddit sin registro, F-Droid)
* [OpenDocument Reader](https://github.com/opendocument-app/OpenDocument.droid) (lector y editor de documentos)
* [Sanmill](https://github.com/calcitem/Sanmill) (juego del molino)
* [Hypatia](https://gitlab.com/divested-mobile/hypatia) (scanner antimalware)
* [Libretra](https://gitlab.com/divested-mobile/libretra) (asistente para reemplazar DNS sin restricciones)
* [PackageManager](https://github.com/SmartPack/PackageManager) (administrador de paquetes de aplicaciones para expertos)
* [NoiseCapture](https://github.com/Ifsttar/NoiseCapture/) (detección de ruido ambiental)
* [Medilog](https://gitlab.com/toz12/medilog) (registro médico)
* [TextSelection WebSearch](https://github.com/zhanghai/TextSelectionWebSearch) (buscador mediante selección de texto)
* [Irregular Expresion](https://github.com/MobileFirstLLC/irregular-expressions) (teclado para transforar el formato de texto, etretenimiento)
* [URL Radio](https://github.com/jamal2362/URL-Radio) (reproductor de radio en línea)
* [SeriesGuide](https://github.com/UweTrottmann/SeriesGuide) (seguimiento de películas y series de televisión)
* [BlueWaller](https://www.transifex.com/bluewallet/bluewallet/) (monedero de Bitcoin)
* [Aria2App](https://github.com/devgianlu/Aria2App) (cliente de aplicación)
* [TableTop](https://github.com/mueller-ma/TabletopTools) (dado virtual)

## Cómo traducir

Recomiendo encontrar aplicaciones desde el repositorio F-droid o [Izzysoft](https://apt.izzysoft.de/fdroid/index.php) donde encontrarán aplicaciones recientes. Eso sí, solo funciona con aplicaciones disponibles para Andorid. En caso que sea multiplataforma, como los escritos en Flutter, no será útil seguir este tutorial por que la app recurre a otras plataformas.

Si en caso que la traducción no tiene una plataforma online, tendrás que copiar los archivos, editar los nuevos y en una carpeta nueva en español ("es") guardarlos.

![Imagen](/img/2020/Carpeta_es_en_Nani_dictionary.png)

En todos los casos debes descargar el comprimido en zip. Después envia por correo o mensaje. Si sabes git mira la nota de abajo.

### Paso a paso

1. Usa el gestor de archivos.
2. Busca la carpeta "app"
3. Dirígete a la carpeta "values" (confirma si existe la carpeta "values-xx")
4. Copia el archivo "strings.xml"
5. Crea una nueva carpeta en "app" llamada "values-es" (al costado de "values")
6. Pega en la carpeta "strings.xml"
7. Abre el archivo hasta que notes las líneas (con un "blog de notas")
8. Antes de traducir, elimina todo lo relacionado con "untranslable" (intraducible, como _translatable="false"_ y que es usado para enlazar como "string/")
9. Empieza a traducir (o mira el paso extra en la siguiente sección)
10. Asegura que las líneas estén bien traduciras
11. Guarda el archivo

Este paso podría repetirse con otras carpetas que llevan el archivo "strings". Por ejemplo, podemos copiar "values-land" a una nueva llamada "values-es-land".

Una vez completado podrás enviar un correo al autor con el archivo guardado. O, si sabes de Git, podrás enviar un pull request. No es un requisito, pero resultará útil Atom para gestionar los git de un repositorio bifurcado.

Adicionalmente, si usas Android Studio basta con descargar el archivo zip y abrir con el [editor de traducción](https://developer.android.com/studio/write/translations-editor).

### Traducción automática desde la PC
Hay una aplicación web gratis, de [código abierto](https://github.com/Ra-Na/GTranslate-strings-xml), sin publicidad y bajo Google Translate. Si no funciona con Google Translate, puedes cambiar la opción y usar DeepL para una traducción más fluida.

1. Accede a [esta web](https://asrt.gluege.boerde.de/)
2. Pega el archivo xml
3. Generará un nuevo archivo xml
4. Guarda el archivo

### Traducción automática desde el móvil
También hay otra [aplicación para móviles](https://github.com/sunilpaulmathew/Translator) llamada ''The Translator''. Es fácil de usar para cualquiera que use un teléfono Android.

1. Consíguelo desde [F-Droid](https://f-droid.org/en/packages/com.sunilpaulmathew.translator/)
2. Abre el archivo xml original
3. Podrás traducir línea por línea
4. Guarda el archivo y compártelo una vez completado

### Fastline

Además de los pasos mostrados, puedes copiar la carpeta "fastline". Sirve para la descripción en la tienda de aplicaciones. En ella encontrarás las subcarpetas "metada y ""android". Estos serían los pasos:

1. Dirígete a la carpeta "android"
2. Dirígete a en-US (o algo así)
3. Copia los archivos "full description" y "short description"
4. Pega en una nueva carpeta llamada "es"
5. Edita esos archivos y tradúcelos al español

***Importante***: Si por error traduciste string por cuerda, corrígelo antes de enviar la solicitud.

## Información adicional

* [Sitio web de Android](https://developer.android.com/guide/topics/resources/localization)
