---
layout: post
title: Cómo crear una lista propia m3u
feature-img: "img/sample_feature_img.png"
---
Este es un tutorial para conseguir la lista de canales en línea de forma gratuita y en el mejor de los casos, fiable. Ten en cuenta que hay limitaciones que suelen traer los proveedores de enlaces, por lo que esta página es informativa. Para empezar, usamos como referencia a TDT Channels un servicio legal para acceder a la televisión española e internacional. Después recurrimos a IPTV-org para añadir otros canales en un grupo especial.

El formato m3u es aquello que facilita la visualización de contenido en vivo como televisión accesible en varias plataformas y libre de regalías. Para garantizar si está en funcionamiento puedes probar con un servicio de edición de m3u en línea y elegir a cual prefieres incluir en ese grupo especial. Por ejemplo está [abp-m3u](https://abp-m3u.tk/) que es gratutito y no requiere registro (excepto para guardar, aunque esta guía no recurre a ello).

Cuando se complete tendrás el archivo mitv.m3u listo para usar en servicio compatible como FastoTV para Android, Kodi para TV, etcétera.

## Cómo elaborar una lista propia m3u

### Ingredientes

* Descarga el archivo TV.m3u desde el repositorio [TDT Channels](https://github.com/LaQuay/TDTChannels). Guarda como mitv.m3u. 
* Luego busca un repositorio que prefieras como [IPTV-org](https://github.com/iptv-org/iptv), que no esta geolocalizado. Escoge un canal formateado y guardalo como mitv2.m3u.
* Consigue un editor de texto compatible con Regex, sino no funcionará. 

### Reagrupar canales locales
1. Para hacer la lista más simple puedes reeemplazar las comunidades por Locales para agruparlas en uno. Si eres de otro país que no sea de España, será bastante cómodo simplificarlo. 
2. Abre el archivo mitv.m3u.
3. Ahora realiza este reemplazo con el buscador. Arriba es de búsqueda y abajo de reemplazo.

´´´
group-title="(?!Int.|Nacionales|Generalistas|Informativos|Musicales|Deportivos|Infantiles|Eventuales|Religiosos|Locales España)[A-Za-z.áéíóñ_ú\- ]*"
group-title="Locales España"
´´´

4. Ahora guárdalo.

### Reagrupar canales modificados
Asegura que deberás transormar el grupo a Int. Modificado para hacer fácil la distinción. El motivo es que deberás añadir los canales personalizados en ese grupo y no interferir cuando necesitas conseguir la lista actualizada de TDTChannels.
5. Abre mitv2.m3u.
6. Ahora rellena estos datos del buscador. Arriba es de búsqueda y abajo de reemplazo.

´´´
group-title="(?!Int.|Nacionales|Musicales|Deportivos|Infantiles|Eventuales|Religiosos|Locales España)[A-Za-z.áéíóñ_ú\- ]*"
group-title="Int. Modificado"
´´´
7. Tras reeemplazarlo a Int. Modificado, copia todo y pégalo al final de texto en el archivo mitv.m3u.
8. Ahora puedes borrar mitv2.m3u.
