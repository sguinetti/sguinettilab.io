---
layout: post
title: Aplicaciones para aprender japones
feature-img: "img/sample_feature_img.png"
---
Aprender japonés no es tan difícil, está lista menciona las aplicaciones disponibles para Android. El objetivo es conocer lo básico sin menor dificultad, como entender el katakana o su pronunciación. Está basado en la selección de F-droid:

* Escritura: [Kakugo](https://f-droid.org/packages/org.kaqui/) (soporta kanji, hiragana y katakana), [Nihonoari](https://f-droid.org/en/packages/com.LAPARCELA.nihonoari/), [Kanjirecord](https://f-droid.org/en/packages/ch.seto.kanjirecog/)
* Bitácora vivencial: [Hibi](https://f-droid.org/en/packages/com.marcdonald.hibi)
* Teclado: [Mozc for Android](https://f-droid.org/en/packages/org.mozc.android.inputmethod.japanese)
* Diccionario: [Sumatora](https://f-droid.org/en/packages/org.happypeng.sumatora.android.sumatoradictionary/), [Jiten](https://github.com/obfusk/jiten) y [Nani](https://f-droid.org/en/packages/eu.lepiller.nani/)
