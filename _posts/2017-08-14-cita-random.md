---
layout: post
title: Cita random
feature-img: "img/sample_feature_img.png"
---
>Los jóvenes necesitan de mucho cuidado frente a las personas extrañas. Con engaño nos llevan a lugares muy remotos y peligrosos para nuestro bienestar. Por eso, no debemos hacer caso a ninguna persona extraña o debemos ser valientes.

Aquel extraño anuncio de la ciudad, bastante irrelevante. Sin embargo, esta nota lo conservé para reflexionar. Por ejemplo, las producción "El dilema de las redes sociales" hace referencia a la difusión de contenido que forma parte del ámbito privado. Si hay redes que buscan compartir o revivir el momento, hay otras que buscan el ánimo de minas los datos.