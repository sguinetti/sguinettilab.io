---
layout: post
title: Lista de aplicaciones sugeridas
feature-img: "img/sample_feature_img_2.png"
---
Este es una copia del artículo "[Selección de aplicaciones opensource para Android](https://xn--deepinenespaol-1nb.org/blog-post/una-seleccion-de-aplicaciones-open-source-para-android/)". Publicado originalmente en Deepin en Español bajo licencia Creative Commons. La intención es recopilar la lista de aplicaciones F-Droid que se puede conseguir con la ayuda de [Aurora Droid](https://f-droid.org/en/packages/com.aurora.adroid/). Aclaro que si no puedes instalar deberás desactivar la seguridad de Google Play. Lamentablemente, las versiones más recientes obligan a [activar Play Protect](https://support.google.com/googleplay/answer/2812853?hl=es). Si el móvil no lleva ese servicio como un Huawei o un ROM libre, estás de enhorabuena.

Aplicaciones esenciales recomendadas
------------------------------------

Las aplicaciones mostradas a continuación son de uso muy recurrente en el teléfono.

*   [_Amaze_](https://f-droid.org/en/packages/com.amaze.filemanager): **Gestor de archivos** con copiado y pegado y comprensión ZIP. Además, lleva un gestor de aplicaciones para desinstalarlas.
*   [_BaldPhone_](https://f-droid.org/en/packages/com.bald.uriah.baldphone): **Lanzador** diseñado para asemejarse a los teléfonos del 2000 con texto grande y fácil de pulsar. Muestra los accesos, aplicaciones y complementa con utilidades como medicamentos, teléfono, visor de fotos y vídeo, notas, tema nocturno, entre otros.
*   [_OsmAnd_](https://f-droid.org/en/packages/net.osmand.plus): **Visor de mapas**. Funcionan sin conexión.
*   [_FairEmail_](https://f-droid.org/en/packages/eu.faircode.email/): **Gestor de correo electrónico**. Soporte para envió y recibo de mensajes e interfaz intuitiva. No sincroniza otros datos, sugerimos usar también _DAVx⁵_.
*   [_LeafPic_](https://f-droid.org/en/packages/com.alienpants.leafpicrevived/): **Visor de fotos** con editor incorporado. La versión actual lleva el apellido "Revived".
*   [_Forecastie_](https://f-droid.org/en/packages/cz.martykan.forecastie): **Pronóstico del clima** basado en OpenWeatherMap. Muestra el clima de hoy con los próximos días. Gráficos y mapa del tiempo incluido.
*   [_OpenBoard_](https://f-droid.org/en/packages/org.dslul.openboard.inputmethod.latin): **Teclado virtual** con sugerencias de escritura.
*   [_QKSMS_](https://f-droid.org/en/packages/com.moez.QKSMS): **Mensajes SMS**. Con respaldo, envio masivo y bloqueo de números no deseados.
*   [_Vinyl Music Player_](https://f-droid.org/en/packages/com.poupa.vinylmusicplayer): **Reproductor de música** con pestañas. Permite escaneo de canciones, establecer ganancia de sonido y lectura de tarjetas SD.
*   [_VLC_](https://f-droid.org/en/packages/org.videolan.vlc): **Reproductor de audio y vídeo**. Es completa por su amplio soporte de códecs.
*   _Bromite_ (disponible en el repositorio de Bromite): **Navegador web** basado en Chromium con bloqueador de anuncios.
*   [**_Calculadora_**](https://f-droid.org/es/packages/com.xlythe.calculator.material/): Opera funciones matemáticas básicas.

Aplicaciones complementarias sugeridas
--------------------------------------

Esta lista no es muy indispensable a diferencia de las anteriores. Por lo tanto, algunos de ellos son útiles para determinados usuarios.

*   _[Feeel](https://f-droid.org/en/packages/com.enjoyingfoss.feeel)_: Ruta de ejercicios. Uno de ellos es la [rutina de aeróbicos en 7 minutos al día](https://www.elconfidencial.com/alma-corazon-vida/2017-05-11/plan-ejercicio-7-minutos-fitness-adelgazar_1379079/), cuyos pasos son simples de memorizar, con 30 segundos por actividad más 10 de descanso. Si necesitas orientación existen varios [vídeos al respecto](https://www.youtube.com/watch?v=sN6XgeeyXlk).
*   _[OpenFoodFacts](https://f-droid.org/en/packages/openfoodfacts.github.scrachx.openfood)_: [Base de datos que lleva su nombre](https://en.wikipedia.org/wiki/Open_Food_Facts), iniciativa de la comunidad con información de más del millón de productos alimenticios.
*   _[Finder](https://f-droid.org/en/packages/ru.seva.finder)_: Aplicación para localizar el móvil y enviar coordenadas por SMS a un número de confianza en caso que sea robado.
*   _[Librera Reader](https://f-droid.org/en/packages/com.foobnix.pro.pdf.reader)_: **Lector de ebooks y PDF**. Con biblioteca y tipos de lectura por toque o deslizado. La versión contiene todas las características de la edición PRO de Google Play.
*   _[LibreTorrent](https://f-droid.org/en/packages/org.proninyaroslav.libretorrent)_: Gestor de torrent. Opción para medir la velocidad.
*   _[MoneyWallet](https://f-droid.org/en/packages/com.oriondev.moneywallet)_: Organizador financiero. Muestra las transacciones y transferencias de forma cronológica. Indica cuales son las deudas, el origen del dinero y las categorías.
*   [_Scarlet Notes FD_](https://f-droid.org/en/packages/com.bijoysingh.quicknote): **Adicción de notas**. Incluye cuaderno, buscador, editor multiformato, bloqueo de acceso y buscador.
*   [_Timetable_](https://f-droid.org/en/packages/com.asdoi.timetable): Cursos y horarios de la escuela, colegio o universidad. Muestra los resúmenes, tareas, apuntes y exámenes. Además añade notificaciones diarias y copia de seguridad.
*   [_Stocks Widget_](https://f-droid.org/en/packages/com.github.premnirmal.tickerwidget): Este muestra el estado de las acciones financieras de forma básica.
*   [_OpenTasks_](https://f-droid.org/es/packages/org.dmfs.tasks/): **Realizador de tareas** con fecha límite.

Aplicaciones para sincronización
--------------------------------

Resulta útiles para administrar información de productividad.

*   [_DAVx⁵_](https://f-droid.org/es/packages/at.bitfire.davdroid/): **Sincronizador de cuentas varias**: contacto, tareas, calendario y [otros servicios](https://en.wikipedia.org/wiki/WebDAV). Para correo, sugerimos integrar con _Fairmail_.
*   [_DecSync_](https://f-droid.org/en/packages/org.decsync.cc): Transfiere contactos y calendario entre el móvil y la PC. Para eso deberás configurar con algún cliente compatible. En este caso puedes [instalar este paquete](https://github.com/39aldo39/Evolution-DecSync/releases) para Deepin que guarda los datos en Evolution.

Aplicaciones para servicios en línea sugeridas
----------------------------------------------

Otros servicios en línea, que resaltan algunas redes sociales.

*   _[NewPipe](https://f-droid.org/en/packages/org.schabi.newpipe)_: Cliente alternativo de YouTube. Permite visualizar vídeos a pantalla completa o ventana, guardado local de listas y categorizar los canales para mostrar sus últimos vídeos.
*   _[Twire](https://f-droid.org/en/packages/com.perflyst.twire)_: Cliente alternativo de Twitch, competencia de YouTube. Está enfocado en los videojuegos pero puedes interactuar las últimas transmisiones en línea.
*   _[Telegram](https://f-droid.org/en/packages/org.telegram.messenger)_: Aplicación de mensajería. Basada en la nube con varias características.
*   _[Aurora Store](https://f-droid.org/en/packages/com.aurora.store)_: Cliente de Google Play para aplicaciones no disponibles en F-Droid. Se puede acceder con una cuenta de invitado, que salta la obligación de crear una cuenta Google.
*   _[PassAndroid](https://f-droid.org/en/packages/org.ligi.passandroid)_: Cliente para descargar y mostrar pases personales, carné o boletos virtuales. Compatible con Apple Wallet (antes Passbook) y esPass.
*   _[MGit](https://f-droid.org/en/packages/com.manichord.mgit/)_: Cliente para repositorios git. Ideal para desarrolladores.
*   [_AntennaPod_](https://f-droid.org/en/packages/de.danoeh.antennapod): Servicio para gestionar podcasts.
*   _[Kiwix](https://f-droid.org/en/packages/org.kiwix.kiwixmobile/)_: Diccionario y/o enciclopedia sin conexión. Funciona con los datos de Wikipedia.

Menciones especiales
--------------------

Es completamente opcional. Solo señalamos a aquellos que podrían ser de interés en el futuro.

*   Las aplicaciones básicas de [Axet](https://gitlab.com/axet), [Simple Mobile Tools](https://www.simplemobiletools.com/): Incluyen gestor de archivos, alarma, mapas y grabador de llamadas. Los autores indicados llevan sus diseños muy minimalistas y cumplen con acciones básicas.
*   [_KDE Connect_](https://xn--deepinenespaol-1nb.org/kde-connect-en-deepin/): Para interactuar entre Deepin y la PC.
*   Además del repositorio recomendado de la tienda de Aurora configurada, podemos añadir otras como _Bitwarden_, _Bromite_, _Guardian Project_ y _KDE Android_, _LibRetro_ y _Nesyms_. Por ejemplo:
    *   _KDE para Android_ (disponible por el [repositorio KDE](https://community.kde.org/Android/FDroid)): Repositorio en desarrollo. Incluye a Plasma Phonebook (lista de contactos), Calindori (calendario), Buho (notas), Koko (lanzador), entre otros.
    *   _Bitwarden_ (repositorio que lleva su nombre): Software para guardar contraseñas con cifrado en la nube.
    *   _LibRetro_ (disponible por separado): Emulador de juegos. Soporta varias consolas de antigua generación, si el móvil lo soporta.
    *   _Save_ (de Guardian): Añade metadatos para subir contenido original.